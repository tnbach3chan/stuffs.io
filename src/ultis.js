import firebase from "./firebase";
import data from "./data.json";

const { blackCards, whiteCards } = data;
const updateTurn = (curTurn, maxPlayers) =>
    curTurn === maxPlayers - 1 ? 0 : curTurn + 1;

const updateRealtimeData = (location, updateField) =>
    firebase.database().ref(location).update(updateField);

const getRealtimeData = (location) =>
    firebase.database().ref(location).once("value");

const getPlayersCard = (amount) => {
    return pickCard(amount, whiteCards);
};
const pickNewQuest = () => {
    return pickCard(1, blackCards)[0].text;
};

const createNewRoom = async (playerId, ownerData, gameType) => {
    const db = firebase.database().ref().push();
    let gameData = {};
    switch (gameType.toUpperCase()) {
        case "ODD": {
            gameData = {
                roundCount: 1,
                answers: "",
                judgeTurn: 0,
                winner: "",
                quest: pickCard(1, blackCards)[0].text,
                roundWinAns: "",
            };
            break;
        }
        default:
            break;
    }
    await db.set({
        gameStart : false,
        gameType: gameType,
        gameData: gameData,
        playersData: { [playerId]: ownerData },
    });
    return (await db).key;
};

function randomId(length) {
    const id = [];
    for (let i = 0; i < length; i++) {
        id.push(Math.floor(Math.random() * length));
    }
    return id.join("");
}
function pickCard(amount, soureCards) {
    const cards = [];
    for (let i = 0; i < amount; i++) {
        const randomId = Math.floor(Math.random() * soureCards.length);
        cards.push(soureCards[randomId]);
    }
    return cards;
}

export {
    pickNewQuest,
    updateTurn,
    createNewRoom,
    randomId,
    pickCard,
    getRealtimeData,
    updateRealtimeData,
    getPlayersCard,
};
