import React from "react";
export default function PlayersInfo({ playersData }) {
    const currentPlayerId = sessionStorage.getItem("playerId");
    return (
        <div
            style={{
                flex: 4,
                position: "relative",
                height: "100vh",
                padding: "40px 60px 40px 60px",
                color: "#305D7A",
                lineHeight: "2",
                fontSize: "1.25em",
            }}
        >
            <h2>Players</h2>
            {playersData &&
                Object.keys(playersData).map((playerId, index) => {
                    const { username } = playersData[playerId];
                    return (
                        <div
                            key={index}
                            style={{
                                fontSize: "1.5em",
                                color:
                                    playerId === currentPlayerId && "#F26628",
                            }}
                        >
                            <h4>
                                {username}{" "}
                                {playerId === currentPlayerId && `(you)`}
                            </h4>
                        </div>
                    );
                })}
        </div>
    );
}
