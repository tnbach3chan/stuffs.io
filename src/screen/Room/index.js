import LoadingData from "../../Components/LoadingData";
import React from "react";

import { useParams } from "react-router-dom";
import useFirebaseRTD from "../../Hooks/useFirebaseRTD";
import withOnePlayerInRoom from "../../Components/withOnePlayerInRoom"
import withMoreThanOnePlayerInRoom from "../../Components/withMoreThanOnePlayerInRoom"
import UI from './UI'
export default function Room() {
    const { id } = useParams();
    const data = useFirebaseRTD(id);
    const { playersData, gameStart } = data;
    const ids = Object.keys(playersData);
    let Room;
    switch (ids.length) {
        case 1:
            Room = withOnePlayerInRoom(id)(UI);
            break;
        case 0:
            Room = LoadingData;
            break;
        default:
            Room = withMoreThanOnePlayerInRoom(id)(UI)
            break;
    }
    return <Room playersData={playersData} gameStart={gameStart} id={id} />;
}
