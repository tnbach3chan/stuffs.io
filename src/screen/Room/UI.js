import React, { useEffect, useState } from "react";
import { useHistory } from "react-router";
import SideBar from "../../Components/Sidebar";
import { updateRealtimeData } from "../../ultis";
import PlayersInfo from "./PlayersInfo";
import { CopyToClipboard } from "react-copy-to-clipboard";
const styles = {
    width: "60%",
};
export default function UI({ playersData, id, gameStart }) {
    const title = "ODDS! The cardgame";
    const history = useHistory();
    useEffect(() => {
        if (gameStart) history.push(`/Odd/${id}`);
    }, [gameStart, id, history]);
    const handleGo = () => {
        updateRealtimeData(`/${id}/`, {
            gameStart: true,
        }).then(() => console.log("hello"));
    };
    const [codeHover, setCodeHover] = useState(false);
    const [linkHover,setLinkHover] = useState(false)
    return (
        <div className="container" style={{ height: "100vh", width: "100vw" }}>
            <SideBar />
            <PlayersInfo playersData={playersData} />
            <div
                style={{
                    flex: 7,
                    background: "black",
                    color: "white",
                    borderRadius: "50px 0px 0px 50px",
                    height: "100vh",
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                    justifyContent: "center",
                    padding: "32px 0",
                }}
            >
                <h1 style={{ margin: 0 }}> {title} </h1>
                <button
                    style={{
                        width: "40%",
                        height: "10%",
                        background: "#F26628",
                        fontSize: "36px",
                        fontWeight: "bold",
                        borderRadius: "20px",
                        color: "white",
                    }}
                    onClick={handleGo}
                >
                    Start Game
                </button>

                <div style={{ width: "70%" }}>
                    <h3>Invite your friends !!</h3>
                    <div
                        style={{
                            backgroundColor: "#F26628",
                            color: "white",
                            display: "flex",
                            width: "100%",
                            borderRadius: "10px",
                            marginBottom: "1rem",
                            alignItems : "center"
                        }}
                    >
                        {codeHover ? (
                            <h4
                                style={{
                                    flexGrow: 1,
                                    textAlign: "center",
                                    margin : 0
                                }}
                                onMouseOut={() => setCodeHover(false)}
                            >
                                {id}
                            </h4>
                        ) : (
                            <h4
                                onMouseOver={() => setCodeHover(true)}
                                style={{ flexGrow: 1, textAlign: "center", margin : 0 }}
                            >
                                Hover to see the 6-digit code
                            </h4>
                        )}
                        <CopyToClipboard text={id}>
                            <button
                                style={{
                                    backgroundColor: "#F26628",
                                    color: "white",
                                    border: 0,
                                    borderRadius: "10px",
                                   
                                }}
                            ><h4>Copy</h4>
                            </button>
                        </CopyToClipboard>
                    </div>
                    <div
                        style={{
                            backgroundColor: "#F26628",
                            color: "white",
                            display: "flex",
                            width: "100%",
                            borderRadius: "10px",
                            marginBottom: "1rem",
                            alignItems : "center"
                        }}
                    >
                        {linkHover ? (
                            <h4
                                style={{
                                    flexGrow: 1,
                                    textAlign: "center",
                                    margin : 0
                                }}
                                onMouseOut={() => setLinkHover(false)}
                            >
                                {`${window.location.hostname}/?${id}`}
                            </h4>
                        ) : (
                            <h4
                                onMouseOver={() => setLinkHover(true)}
                                style={{ flexGrow: 1, textAlign: "center", margin : 0 }}
                            >
                                Hover to see the direct invite link


                            </h4>
                        )}
                        <CopyToClipboard text={`${window.location.hostname}/?${id}`}>
                            <button
                                style={{
                                    backgroundColor: "#F26628",
                                    color: "white",
                                    border: 0,
                                    borderRadius: "10px",
                                   
                                }}
                            ><h4>Copy</h4>
                            </button>
                        </CopyToClipboard>
                    </div>
                </div>
            </div>
        </div>
    );
}
