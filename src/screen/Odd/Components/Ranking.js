import React from "react";
import RankItem from "./RankItem";

export default function Ranking({players}) {
    return (
        <>
            <div style={{width : "100%", height : "50%"}}>
                {Object.keys(players).map((key,index) => {
                    const { points, username } = players[key];
                    return (
                        <RankItem
                            key={index}
                            rank={index + 1}
                            username={username}
                            point={points}
                        />
                    );
                })}
            </div>
        </>
    );
}
