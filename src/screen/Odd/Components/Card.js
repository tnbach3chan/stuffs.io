import React, { useState } from "react";
export default function Card({ ans, index, drawCard }) {
    const [active, setActive] = useState(false);
    const [played, setPlayed] = useState(false);
    const width = 15;
    const amountPlayer = 7;
    return (
        <>
            <div
                style={{
                    textAlign: "center",
                    display: active ? "block" : "none",
                    position: "absolute",
                    top: "55%",
                    left: "40%",
                }}
            >
                Click to play this card
            </div>
            <div
                style={{
                    position: "absolute",
                    width: `${width}%`,
                    height: "22%",
                    left: `${
                        (100 - ((amountPlayer + 1) * width) / 1.5) / 2 +
                        (width / 1.5) * index
                    }%`,
                    bottom: active ? "0%" : "-5%",
                    background: active ? "#F26628" : "#8F8F8F",
                    boxShadow: "-6px 0px 16px rgba(0, 0, 0, 0.25)",
                    borderRadius: "15px 15px 0px 0px",
                    padding: "2%",
                    zIndex: active ? 100000 : index,
                    transition: "bottom 0.7s",
                    cursor: "pointer",
                    display: played && "none",
                }}
                onMouseOver={() => setActive(true)}
                onMouseLeave={() => setActive(false)}
                onClick={() => {
                    drawCard(ans);
                    setPlayed(true);
                }}
            >
                {ans}
            </div>
        </>
    );
}
