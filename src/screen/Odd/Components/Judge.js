import React from "react";
export default function Judge({ judgeName, allAnswered }) {
    return (
        <>
            <div>
                <strong>{judgeName}</strong> is the{" "}
                <strong style={{ fontWeight: 600 }}>Judge</strong> this round
            </div>
            {!allAnswered && (
                <div
                    style={{
                        textAlign: "right",
                    }}
                >
                    Wating for others to choose their card
                    <span id="loading"></span>
                </div>
            )}
        </>
    );
}
