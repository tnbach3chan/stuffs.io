import React from "react";
import Card from "./Card";

export default function Cards({ cards, drawCard}) {
    return (
        <>
            {cards.map((ans, index) => {
                return (
                    <Card
                        drawCard={drawCard}
                        index={index}
                        ans={ans}
                        key={index}
                    />
                );
            })}
        </>
    );
}
