import React from "react";
const styles = {
    quest: {
        fontWeight: 600,
        color: "black",
    },
    round: {
        textAlign: "center",
        color: "#F26628",
    },
    questContainer: {
        background: "white",
        borderRadius: "30px",
        padding: "5%",
    },
};
export default function Quest({quest,roundCount}) {
    return (
        <>
            <div style={styles.questContainer}>
                <div style={styles.round}>ROUND {roundCount}</div>
                <div style={styles.quest}>{quest}</div>
            </div>
        </>
    );
}
