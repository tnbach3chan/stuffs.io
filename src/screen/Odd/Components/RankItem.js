import React from "react"

const styles = {
    rankItem: {
        display: "flex",
        flex : 1
    },
    rank: {
        flex : 2,
        justifyContent : "flex-end",
        color: "#ffffff",
        display : "flex",
        alignItems : "center"
    },
    user : {
        flex : 6
    }
}
export default function RankItem({ rank, point, username }) {
    return (
        <div style={styles.rankItem}>
            <div style={styles.rank}>
                <strong style={{marginRight : "10px"}}>#{rank}</strong>    
            </div>
            <div style={styles.user}>
                <span style={{ display: "block", color: "#FFFFFF" }}>
                    {username}
                </span>
                <span style={{ color: "#8F8F8F" }}>Points : {point}</span>
            </div>
        </div>
    );
}