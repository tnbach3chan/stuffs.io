import React from "react";
import Answer from "./Answer";

export default function Answers({ answers, isJudge, setRoundWinner,roundWinAns }) {
    return (
        <div style={{ left: "28%", position: "absolute", top: "58%" }}>
            <h4>Answers :</h4>
            {answers.map((item, index) => (
                <Answer
                    setRoundWinner={setRoundWinner} 
                    roundWinAns={roundWinAns}
                    isJudge={isJudge}
                    ans={item}
                    key={index}
                    id={index}
                />
            ))}
        </div>
    );
}
