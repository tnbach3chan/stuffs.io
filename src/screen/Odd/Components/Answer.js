import React, { useState } from "react";

export default function Answer({ id, ans,isJudge,setRoundWinner,roundWinAns }) {
    const [color,setColor] = useState("white")
    return (
        <button
            style={{
                color: ans === roundWinAns ? "#F26628" :  color,
                cursor : "pointer",
                display : "block",
                background: "transparent",
                width : "100%",
                height : "40px",
                border : "none"
            }}
            disabled = {!isJudge}
            onClick={() => {
                setRoundWinner(ans)
            }}
            onMouseOver={() => setColor("#F26628")}
            onMouseLeave={() => setColor("white")}
        >
            <strong>
                {id + 1}. {ans}
            </strong>
            <span style={{display : ans === roundWinAns ? "inline" : "none"}}>{` (winner)`}</span>
        </button>
    );
}
