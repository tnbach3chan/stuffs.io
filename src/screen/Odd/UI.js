import React from "react";
import Sidebar from "../../Components/Sidebar";
import GameField from "./GameField";
function UI(props) {
    return (
        <div
            style={{
                width: "100vw",
                height: "100vh",
                backgroundColor: "black",
                display: "flex",
            }}
        >
            <Sidebar />
            <GameField {...props} />
        </div>
    );
}
export default UI;
