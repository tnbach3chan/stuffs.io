import LoadingData from "../../Components/LoadingData";
import React from "react";

import { useParams } from "react-router-dom";
import useFirebaseRTD from "../../Hooks/useFirebaseRTD";
import withOnePlayerInRoom from "../../Components/withOnePlayerInRoom"
import withMoreThanOnePlayerInRoom from "../../Components/withMoreThanOnePlayerInRoom"
import UI from "./UI"
export default function Odd() {
    const { id } = useParams();
    const data = useFirebaseRTD(id)
    const { playersData, gameData } = data;
    const ids = Object.keys(playersData);
    let Odd;
    switch (ids.length) {
        case 1:
            Odd = withOnePlayerInRoom(id)(UI)
            break;
        case 0:
            Odd = LoadingData;
            break;
        default:
            Odd = withMoreThanOnePlayerInRoom(id)(UI)
            break;
    }
    return (
        <Odd playersData={playersData} gameData={gameData} id={id}/>
    );
}

