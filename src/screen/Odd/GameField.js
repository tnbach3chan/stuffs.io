import React from "react";
import Ranking from "./Components/Ranking";
import Quest from "./Components/Quest";
import Judge from "./Components/Judge";
import Arrow from "../../Components/Arrow";
import styles from "./Odd.module.css";
import { Link } from "react-router-dom";
import Answers from "./Components/Answers";
import Cards from "./Components/Cards";
import clsx from "clsx";
import { CSSTransition } from "react-transition-group";
import { pickNewQuest, updateTurn, updateRealtimeData } from "../../ultis";
const getCurrentPlayerData = (playersData) => {
    const playerId = sessionStorage.getItem("playerId");
    return playersData[playerId];
};

const getJudgeInfo = (playersData, judgeTurn, answers) => {
    const playerId = sessionStorage.getItem("playerId");
    const ids = Object.keys(playersData);
    const judgeId = ids[judgeTurn];

    const judgeName = playersData[judgeId].username;
    const isJudge = judgeId === playerId;
    const allAnswered =
        answers.length === ids.length - 1 && Array.isArray(answers);
    return { judgeName, isJudge, allAnswered, numberOfPlayers: ids.length };
};
export default function GameField({playersData, gameData, id}) {
    const { quest, answers, judgeTurn, roundWinAns, roundCount } = gameData;
    const currentPlayerData = getCurrentPlayerData(playersData);
    const { judgeName, isJudge, allAnswered, numberOfPlayers } = getJudgeInfo(
        playersData,
        judgeTurn,
        answers
    );
    // logic play games
    const playersDataLoca = `/${id}/playersData`;
    const gameDataLoca = `/${id}/gameData`;
    const drawCard = (ans) => {
        if (answers.length === 0)
            updateRealtimeData(gameDataLoca, { answers: [ans] });
        else {
            answers.push(ans);
            updateRealtimeData(gameDataLoca, { answers: answers });
        }
    };

    const setRoundWinner = async (ans) => {
        const newPlayersData = Object.assign({}, playersData);
        for (const id in newPlayersData) {
            const { cards } = newPlayersData[id];
            let newPoints = newPlayersData[id].points;
            const newCards = cards.filter((data) => data !== ans);
            if (newCards.length < cards.length) newPoints += 10;
            newPlayersData[id] = {
                ...newPlayersData[id],
                cards: newCards,
                points: newPoints,
            };
        }
        await updateRealtimeData(playersDataLoca, newPlayersData);
        await updateRealtimeData(gameDataLoca, {
            roundWinAns: ans,
        });
        await setTimeout(() => {
            updateRealtimeData(gameDataLoca, {
                //write new pick quest
                roundCount: roundCount + 1,
                quest: pickNewQuest(),
                answers: "",
                judgeTurn: updateTurn(judgeTurn, numberOfPlayers),
            });
        }, 3000);
    };
    return (
        // <CSSTransition
        //     unmountOnExit
        //     in={true}
        //     timeout={{ appear: 0, enter: 0, exit: 300 }}
        //     classNames={{
        //         appear: styles.rollAppear,
        //         enter: styles.rollEnter,
        //         enterDone: styles.rollEnterDone,
        //         exit: styles.rollExit,
        //         exitActive: styles.rollExitActive,
        //     }}
        //     appear
        // >
            <div className={styles.container}>
                <Link to="/">
                    <Arrow
                        fill="#F26628"
                        style={{
                            position: "absolute",
                            left: "4%",
                            top: "7%",
                            width: "4%",
                            cursor: "pointer",
                            transform: "rotate(180deg)",
                        }}
                    />
                </Link>
                <div className={styles.info}>
                    <div className={clsx(styles.ranking, styles.item)}>
                        <Ranking players={playersData} />
                    </div>
                    <div className={clsx(styles.quest, styles.item)}>
                        <Quest roundCount={roundCount} quest={quest} />
                    </div>
                    <div className={clsx(styles.judge, styles.item)}>
                        <Judge
                            allAnswered={allAnswered}
                            judgeName={judgeName}
                        />
                    </div>
                </div>
                <div className={styles.answer}>
                    {allAnswered ? (
                        <Answers
                            roundWinAns={roundWinAns}
                            setRoundWinner={isJudge && setRoundWinner}
                            answers={answers}
                            isJudge={isJudge}
                        />
                    ) : (
                        !isJudge && (
                            <Cards
                                drawCard={drawCard}
                                cards={currentPlayerData.cards}
                            />
                        )
                    )}
                </div>
            </div>
        // </CSSTransition>
    );
}
