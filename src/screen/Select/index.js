import React, { useReducer, useState } from "react";
import SideBar from "../../Components/Sidebar";
import { useLocation,useHistory } from "react-router-dom";
import {
    createNewRoom,
    randomId,
    updateRealtimeData,
    getRealtimeData,
    getPlayersCard,
} from "../../ultis";
import style from "./Select.module.css";
import GameList from "./GameList";
import Arrow from "../../Components/Arrow"
const styles = {
    width: "50%",
};
const initialState = {count: 0};

function reducer(state, action) {
  switch (action.type) {
    case 'increment':
      return {count: state.count + 1};
    case 'decrement':
      return {count: state.count - 1};
    default:
      throw new Error();
  }
}
const Select = () => {
    const gameList = ["ODD","PONG"]
    const history = useHistory()
    const location = useLocation();
    const username = location.search.slice(1, location.search.length);
    const [isJoin, setIsJoin] = useState(false);
    const [roomId, setRoomId] = useState("");
    const title = "ODDS! The cardgame";
    const playerData = {
        username: username,
        points: 0,
        //game   = ODD
        cards: getPlayersCard(7),
    };
    const [state,dispatch] = useReducer(reducer,initialState)
    const creatRoom = async () => {
        const playerId = randomId(10);
        sessionStorage.setItem("playerId", playerId);
        const newRoomId = await createNewRoom(playerId, playerData,gameList[state.count]);
        history.push(`/room/${newRoomId}`)
    };
    const joinGame = async () => {
        const playerId = randomId(10);
        sessionStorage.setItem("playerId", playerId);
        const info = await getRealtimeData(`${roomId}/playersData`);
        console.log(info.val());
        const playersData = info.val();
        await updateRealtimeData(`${roomId}/playersData`, {
            ...playersData,
            [playerId]: playerData,
        });
        history.push(`/room/${roomId}`)
    };
    return (
        <div className="container" style={{ height: "100vh", width: "100vw" }}>
            <SideBar />
            <div
                style={{
                    flex: 4,
                    position: "relative",
                    height: "100vh",
                    padding: "40px 60px 40px 60px",
                    color: "#305D7A",
                    lineHeight: "2",
                    fontSize: "1.25em",
                }}
            >
                <h1>Welcome {username}!</h1>
                <h4>Select a game to begin</h4>
                <GameList count={state.count} dispatch={dispatch} gameList={gameList}/>
            </div>
            <div
                style={{
                    flex: 7,
                    background: "black",
                    color: "white",
                    borderRadius: "50px 0px 0px 50px",
                    height: "100vh",
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                    justifyContent: "space-evenly",
                }}
            >
                <h1 style={{ margin: 0 }}> {title} </h1>

                <button
                    style={{
                        ...styles,
                        height: "10%",
                        background: "#F26628",
                        fontSize: "36px",
                        fontWeight: "bold",
                        borderRadius: "20px",
                        color: "white",
                    }}
                    onClick={creatRoom}
                >
                    Create Room
                </button>

                <h1 style={{ margin: 0 }}>Or</h1>

                {isJoin ? (
                    <div
                        style={{
                            display: "inline-flex",
                            ...styles,
                            height: "10%",
                            // justifyContent: "space-between",
                            alignItems : "center"
                        }}
                    >
                        <input
                            placeholder="Input Game ID"
                            style={{
                                width: "80%",
                                height: "100%",
                                fontSize: "18px",
                                borderRadius: "20px",
                                color: "#8F8F8F",
                                backgroundColor: "white",
                                border: "0px solid",
                                padding : "32px",
                            }}
                            onChange={(e) => setRoomId(e.target.value)}
                        />
                        <button
                            onClick={joinGame}
                            style={{
                                backgroundColor : "transparent",
                                border : 0,
                                padding : 0,
                                margin : 0
                            }}
                        >
                            <Arrow fill="#F26628" />
                        </button>
                    </div>
                ) : (
                    <button
                        onClick={() => setIsJoin(true)}
                        style={{
                            ...styles,
                            height: "10%",
                            background: "#FFFFFF",
                            color: "#000000",
                            fontSize: "36px",
                            borderRadius: "20px",
                        }}
                    >
                        Join Existing Room
                    </button>
                )}
                <div
                    style={{
                        color: "#FFFFFF",
                        fontSize: "24px",
                        height: "40%",
                        overflowY: "scroll",
                        padding: "8px",
                        width : "80%"
                    }}
                    className={style.hasScrollBar}
                >
                    <div>
                        <p>
                            {" "}
                            <b>2,133</b> <span>player currently play game</span>{" "}
                        </p>
                    </div>
                    <div>
                        <p>
                            {" "}
                            <b>How to play</b>{" "}
                        </p>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit. Duis aliquet laoreet nisi, vel tincidunt
                            turpis bibendum sed Duis aliquet laoreet nisi, vel
                            tincidunt turpis bibendum sed Duis aliquet laoreet
                            nisi, vel tincidunt turpis bibendum sed Duis aliquet
                            laoreet nisi, vel tincidunt turpis bibendum sed
                        </p>
                    </div>
                </div>
            </div>
        </div>
    );
};
export default Select;
