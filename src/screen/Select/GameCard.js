import React from "react";
const GameCard = ({ name, height ,id}) => {
    return (
        <div
            style={{
                width: "100%",
                height : `${height}px`,
                background: (id%2 === 0) ? "black" : "#F26628",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                color: "white",
                borderRadius: "40px",
            }}
        >
            <div className="gameName">
                <h1>{name.toUpperCase()}</h1>
            </div>
        </div>
    );
};
export default GameCard;
