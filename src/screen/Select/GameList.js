import { DownArrow } from "../../Components/DownArrow";
import {UpArrow } from "../../Components/UpArrow"
import GameCard from "./GameCard";
import React from "react";
import styles from "./Select.module.css"

export default function GameList({count,dispatch,gameList}) {
    const height = 300
    return (
        <div className="center">
            <div className={styles.arrowContainer}>
            <UpArrow onClick = {() => dispatch({type : 'decrement'})} canDisplay={count === 0}/>
            </div>
            
            <div style={{ overflow: "hidden",width : "100%",height : `${height}px` }}>
                <div style={{
                    transition : "all 0.7s",
                    transform : `translateY(-${count * height}px)`
                }}>
                    {gameList.map((game, index) => (
                        <GameCard
                            height={height}
                            name={game}
                            key={index}
                            id={index}
                        />
                    ))}
                </div>
            </div>
            <div className={styles.arrowContainer}>
            <DownArrow canDisplay={count === gameList.length - 1 }onClick = {() => dispatch({type : 'increment'})}/>    
            </div>
            
        </div>
    );
}
