import React from 'react'

const SelectTab = () => {
    const title = 'ODDS! The cardgame'
    return (
        <div style={styles.container}>
            <div style={styles.title}>{title}</div>
            <button style={styles.create}>Create Game</button>
            <div style={styles.or}>Or</div>
        </div>
    )
}

const styles = {
    container: {
        position: 'absolute',
        left: '42.46%',
        right: 0,
        height: '100%',
        background: '#000000',
        borderRadius: '50px 0px 0px 50px',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    title: {
        paddingTop: '60px',
        paddingLeft: '148px',
        fontFamily: 'Poppins',
        fontWeight: '900',
        fontSize: '48px',
        textAlign: 'center',
        color: '#FFFFF'
    },
    create: {
        position: 'absolute',

        // paddingTop: '72px',
        // paddingLeft: '185px',
        marginTop: '204px',
        marginLeft: '185px',

        width: '459px',
        height: '102px',

        background: '#F26628',
        color: '#FFF',
        fontWeight: 'bold',
        borderRadius: '20px',
        fontSize: '36px',
    },
    or: {
        position: 'absolute',
        top: '32..59%',
        right: '%',
        bottom: '61.96%',
    },
    join: {

    }
}

export default SelectTab