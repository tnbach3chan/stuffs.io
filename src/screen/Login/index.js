import React, { useState } from "react";
import { Link, useHistory, useLocation } from "react-router-dom";
import {
    randomId,
    updateRealtimeData,
    getRealtimeData,
    getPlayersCard,
} from "../../ultis";
import styles from "./Login.module.css";

import textLogo from "../../assets/images/stuffs.io.png";
import Logo from "../../Components/Logo";
import Question from "../../Components/Question";
import Arrow from "../../Components/Arrow";

export default function Login() {
    const history = useHistory()
    const location = useLocation();
    const roomId = location.search.slice(1, location.search.length);
    const [username, setUsername] = useState("");
    const [onChange, setOnChange] = useState(false);
    const joinGame = async () => {
        const playerData = {
            username: username,
            points: 0,
            //game   = ODD
            cards: getPlayersCard(7),
        };
        const playerId = randomId(10);
        sessionStorage.setItem("playerId", playerId);
        const info = await getRealtimeData(`${roomId}/playersData`);
        const playersData = info.val();
        await updateRealtimeData(`${roomId}/playersData`, {
            ...playersData,
            [playerId]: playerData,
        });
        history.push(`/room/${roomId}`)
    };
    return (
        <>
            <div className={styles.col1}>
                <span
                    style={{
                        position: "absolute",
                        width: "100%",
                        bottom: "5%",
                        marginLeft: "auto",
                        marginRight: "auto",
                    }}
                >
                    <Question style={{ left: "50%", marginLeft: "25%" }} />
                </span>
            </div>
            <div className={styles.col11}>
                <div className={styles.loginForm}>
                    <Logo
                        style={{
                            display: "block",
                            width: "40%",
                            top: "0%",
                            marginLeft: "auto",
                            marginRight: "auto",
                        }}
                    />
                    <img
                        src={textLogo}
                        className={styles.textLogo}
                        alt="logo"
                    />
                    <div
                        className={styles.inputView}
                        style={{
                            width: onChange ? "70%" : "100%",
                            transition: "width 500ms, ease out",
                        }}
                    >
                        <input
                            type="text"
                            className={styles.inputText}
                            placeholder="Enter your name"
                            value={username}
                            onClick={() => {
                                setOnChange(true);
                            }}
                            onChange={(e) => {
                                setUsername(e.target.value);
                            }}
                        />
                    </div>
                    {roomId ? (
                        <span
                            onClick={joinGame}
                            className={styles.block1}
                        >
                            <Arrow fill="#305D7A" />
                        </span>
                    ) : (
                        <Link to={`/select?${username}`}>
                            <span className={styles.block1}>
                                <Arrow fill="#305D7A" />
                            </span>
                        </Link>
                    )}
                </div>
            </div>
        </>
    );
}
