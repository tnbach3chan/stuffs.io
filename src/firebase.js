import firebase from "firebase/app"
import 'firebase/database'

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyB0-mAgV3LEHhSBw67WB70wDEH9rgRZouE",
  authDomain: "stuffs-io.firebaseapp.com",
  databaseURL: "https://stuffs-io.firebaseio.com",
  projectId: "stuffs-io",
  storageBucket: "stuffs-io.appspot.com",
  messagingSenderId: "435260311656",
  appId: "1:435260311656:web:7f89014f3cba795539d3dd",
  measurementId: "G-PFCMYXS2EC"
};
firebase.initializeApp(firebaseConfig);
export default firebase
