import { useEffect } from "react";

import firebase from "../firebase";

const usePlayerLeaveRoom = (id) => {
    //nhieu hon 1 user trong phong
    useEffect(() => {
        const db = firebase.database().ref(id);
        const playerId = sessionStorage.getItem("playerId");
        db.child("playersData").child(playerId).onDisconnect().remove();
        return () => {
            db.onDisconnect().cancel();
        };
    }, [id]);
};

export default usePlayerLeaveRoom;
