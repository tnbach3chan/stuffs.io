import { useEffect,useState } from "react";

import firebase from "../firebase";

const useFirebaseRTD = (id) => {
    const [data, setData] = useState({ playersData: {}, gameData: {} });
    useEffect(() => {
        const db = firebase.database().ref().child(id);
        db.on("value", (snapshot) => {
            setData(snapshot.val());
        });
    }, [id]);
    return data
};

export default useFirebaseRTD;
