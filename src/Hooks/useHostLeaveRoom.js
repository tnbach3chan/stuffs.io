import  { useEffect } from "react";

import firebase from "../firebase";

const useHostLeaveRoom = (id) => {
    //khi con moi 1 user nguoi trong phong
    useEffect(() => {
        const db = firebase.database().ref().child(id);
        db.onDisconnect().remove()
        return () => {
           db.onDisconnect().cancel()
        };
    }, [id]);
};

export default useHostLeaveRoom;
