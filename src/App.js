import React, {Suspense} from "react";
import { Switch, Route } from "react-router-dom";

import LoadingData from "./Components/LoadingData"
const NotFound = () => <div>Error 404</div>;
const Login = React.lazy(() => import('./screen/Login/index'))
const Select = React.lazy(() => import('./screen/Select/index'))
const Odd = React.lazy(() => import('./screen/Odd/index'))
const Room = React.lazy(() => import('./screen/Room/index'))
function App() {
    return (
        <Suspense fallback={<LoadingData />}>
            <Switch>
                <Route exact path="/">
                    <Login />
                </Route>
                <Route path="/select">
                    <Select />
                </Route>
                <Route path="/room/:id">
                    <Room />
                </Route>
                <Route path="/Odd/:id">
                    <Odd />    
                </Route>

                <Route path="*">
                    <NotFound />
                </Route>
            </Switch>
        </Suspense>
    );
}

export default App;
