import React from "react"
import qMark from "../assets/images/Q&A.png"

export default function Question({style}) {
	return(
		<img src={qMark} style={style} alt="faq"/>
	)
}