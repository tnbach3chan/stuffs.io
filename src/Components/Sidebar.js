import React from "react";
import Logo from "./Logo";
import Question from "./Question";
import { CSSTransition } from "react-transition-group";
import styles from "./Sidebar.module.css";
function SideBar() {
    return (
        <CSSTransition
            unmountOnExit
            in={true}
            timeout={{ appear: 0, enter: 0, exit: 300 }}
            classNames={{
                appear: styles.rollAppear,
                enter: styles.rollEnter,
                enterDone: styles.rollEnterDone,
                exit: styles.rollExit,
                exitActive: styles.rollExitActive,
            }}
            appear
        >
            <div
                style={{
                    flex: 1,
                    background: "white",
                    // position: "relative",
                    height: "100vh",
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "space-between",
                    alignItems: "center",
                    padding: "3rem 0",
                }}
            >
                <Logo
                    style={{
                        width: "50%",
                    }}
                />
                <Question
                    style={{
                        width: "20%",
                    }}
                />
            </div>
        </CSSTransition>
    );
}
const MemoizedSideBar = React.memo(SideBar);
export default MemoizedSideBar;
