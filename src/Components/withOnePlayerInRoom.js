import React from 'react'
import useHostLeaveRoom from "../Hooks/useHostLeaveRoom";
const withOnePlayerInRoom = (id) => (WrappedComponent) => {
    const OnePlayerInRoom = (props) => {
        useHostLeaveRoom(id);
        return <WrappedComponent id={id} {...props} />;
    };
    return OnePlayerInRoom;
};
export default withOnePlayerInRoom;
