import React from "react"
import Logo from "../assets/images/Logo.png"

export default function Question({style}) {
	return(
		<img src={Logo}  style={style} alt="io"/>
	)
}