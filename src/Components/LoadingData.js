import React from "react";
import classes from "./LoadingData.module.css";
export default function LoadingData() {
    return (
        <div className="center" style={{ height: "100vh", width: "100vw",display : "flex" }}>
            <div className={classes["lds-facebook"]}>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    );
}
