import React from "react";
// import arrow from "../../assets/images/Arrow.svg"
// import { useHistory } from "react-router-dom";
export default function Arrow({fill, style,onClick}) {
    // const history = useHistory()
    return (
        <svg width="98" onClick={onClick} height="102" viewBox="0 0 98 102" xmlns="http://www.w3.org/2000/svg" style = {style}>
			<path d="M55.6148 42.0185L62.0383 48.6719H28.9662C27.5313 48.6719 26.402 49.8766 26.402 51.3203C26.402 52.764 27.5313 53.9687 28.9662 53.9687H62.0382L55.6148 60.6221L55.6148 60.6221C54.6204 61.6523 54.6167 63.3183 55.6063 64.3532C56.6069 65.3997 58.2415 65.4034 59.2466 64.3623L58.8869 64.015L59.2466 64.3623L70.0324 53.1904L70.0374 53.1852L70.0398 53.1826C71.0275 52.151 71.025 50.4896 70.042 49.4605L70.0421 49.4604L70.0323 49.4502L59.2465 38.2783C58.2412 37.2371 56.6067 37.2411 55.6062 38.2874C54.6166 39.3223 54.6203 40.9883 55.6147 42.0185L55.6148 42.0185Z" fill={fill} stroke={fill}/>
		</svg>
    );
}
