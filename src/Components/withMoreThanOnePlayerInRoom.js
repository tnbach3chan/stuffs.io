import React from 'react'
import usePlayerLeaveRoom from "../Hooks/usePlayerLeaveRoom"
const withMoreThanOnePlayerInRoom = (id) => (WrappedComponent) => {
    const MoreThanOnePlayerInRoom = (props) => {
        usePlayerLeaveRoom(id)
        return <WrappedComponent {...props}/>
    }
    return MoreThanOnePlayerInRoom
}
export default withMoreThanOnePlayerInRoom